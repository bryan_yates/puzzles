#!/usr/bin/perl -w

use Term::ANSIColor 2.00 qw(:pushpop);

$d = "#"; # dark blue, sorry, no *'s here
$l = "-"; # light blue, sorry, no .'s here
$dc = PUSHCOLOR BRIGHT_BLUE ON_BRIGHT_BLUE ".", RESET;
$lc = PUSHCOLOR BOLD BLACK ON_BRIGHT_BLUE "#", RESET;

$u = "u"; # next generation dark blue marker
$n = "n"; # newline marker within the string
$s = " "; # space, change only for debugging

$a = "$d"; # GLOBAL, manipulated throughout
pad(); # add one extra space loop first time

while (++$i) {
	#~ system("cls"); # or "clear" in linux/osx
	$p = $a;
	$p =~ s/$n/\n/g;
	$p =~ s/$d/$dc/g;
	$p =~ s/$l/$lc/g;
	print "$p\n";

	print "Generation $i\nHit enter to continue";
	$input = <>;
	# process a new iteration
	# grow square by 1 more concentric square
	pad();
	grow();
}

sub pad {
	# fix the vertical size first
	$sp = $a;
	$sp =~ s/$n.*//;
	$sp =~ s/./$s/;
	$a =~ s/^/$sp$n/;
	$a =~ s/$/$n$sp/;
	# now fix the horizonal
	$a =~ s/$n/$s$n$s/g;
	$a =~ s/^/$s/g;
	$a =~ s/$/$s/g;
}

sub grow {
	# this visually represents this s/r series
	# the x represents a light or dark blue square that may have growth around it, at A, B, C, and/or D
	# the | represents a left and right margin that will be present
	# The total acutal spacing for vertical alignment (from A to x) will be the width plus width of $n
	# _ MUST be spaces for [ABCD] to be new dark blue, .'s can be anything
	# 
	# |||.._..|||
	# |||._A_.|||
	# |||_DxB_|||
	# |||._C_.|||
	# |||.._..|||
	
	$b = $a;
	$b =~ s/$n.*//;
	$r = length("$b$n") - 5;
	do {
		$t = $a;
		$a =~ s/([$u$s]...{$r}.[$u$s])$s([$u$s]..{$r}..[$l$d])/$1$u$2/g; # A
		$a =~ s/([$u$s]..{$r}..[$l$d])$s([$u$s].{$r}...[$u$s])/$1$u$2/g; # B
		$a =~ s/([$l$d]...{$r}.[$u$s])$s([$u$s]..{$r}..[$u$s])/$1$u$2/g; # C
		$a =~ s/([$u$s]....{$r}[$u$s])$s([$l$d]...{$r}.[$u$s])/$1$u$2/g; # D
	} until ($t eq $a);
	$a =~ s/$l/$s/g;
	$a =~ s/$d/$l/g;
	$a =~ s/$u/$d/g;
}
